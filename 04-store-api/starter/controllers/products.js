const Product = require('../models/product');

const getAllProductsStatic = async (req, res, next) => {
  const search = 'wo';
  //   throw new Error('testing async errors')//require the package in app.js must!
  //   next(new Error('testing async errors'));
  // const products = await Product.find({}).sort('-name price');//if has the same name so the less price is before the higher
  const products = await Product.find({}).select('name price -_id');

  res.status(200).json({ products, nbHits: products.length });
};

const getAllProducts = async (req, res) => {
  const { featured, company, name, sort, fields, numericFilters } = req.query;
  const queryObject = {};
  if (featured) {
    queryObject.featured = featured === 'true' ? true : false;
  }
  if (company) {
    queryObject.company = company;
  }
  if (name) {
    queryObject.name = { $regex: name, $options: 'i' }; //i >> uppercase or lowercase
  }

  // console.log(queryObject);

  let result = Product.find(queryObject);
  if (sort) {
    const sortList = sort.split(',').join(' ');
    result = result.sort(sortList);
  } else {
    result = result.sort('createdAt');
  }
  if (fields) {
    const fieldList = fields.split(',').join(' ');
    result = result.select(fieldList);
  }
  if (numericFilters) {
    const operatorMap = {
      '>': '$gt',
      '>=': '$gte',
      '<': '$lt',
      '<=': '$lte',
      '=': '$eq',
    };
    const regex = /\b(<|>|<=|>=|=)\b/g;
    let filters = numericFilters.replace(regex, (match) => {
      return `-${operatorMap[match]}-`;
    });
    // console.log(filters);//price-$gt-20,rating-$gt-3
    const options = ['price', 'rating'];

    const numericFiltersList = filters.split(',').forEach((item) => {
      // console.log(item.split('-'));//[ 'price', '$gt', '20' ][ 'rating', '$gt', '3' ]
      const [field, operator, value] = item.split('-');
      if (options.includes(field)) {
        console.log({ [operator]: Number(value) });
        queryObject[field] = { [operator]: Number(value) }; //dynamically changed
        // queryObject.field={[operator]:Number(value)}//means that there is a property called field
        // console.log('q='+queryObject.field);
      }
    });
    console.log(queryObject);

    result = result.find(queryObject);
  }
  const page = Number(req.query.page) || 1;
  const limit = Number(req.query.limit) || 10;
  const skip = (page - 1) * limit;

  result = result.skip(skip).limit(limit);
  const products = await result;
  res.status(200).json({ products, nbHits: products.length });
};

module.exports = {
  getAllProducts,
  getAllProductsStatic,
};
