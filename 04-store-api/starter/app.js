require('dotenv').config();
require('express-async-errors')
//async errors
const express = require('express');
const app = express();
const connectDB = require('./db/connect');
const productsRouter = require('./routes/products');

//middleware
app.use(express.json());
const notFoundMiddleWare = require('./middleware/not-found');
const errorHandlerMiddleWare = require('./middleware/error-handler');
//routes
app.get('/', (req, res, next) => {
  res.send('<h1>Store Api</h1><a href="/api/v1/products">product route</a>');
});

app.use('/api/v1/products', productsRouter);

app.use(notFoundMiddleWare);
app.use(errorHandlerMiddleWare);

const port = 3000 || process.env.PORT;
const start = async () => {
  try {
    await connectDB(process.env.MONGO_URL);
    app.listen(port, console.log('server is on'));
  } catch (err) {
    console.log(err);
    // next(err);
  }
};
start();
