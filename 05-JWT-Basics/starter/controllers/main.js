const jwt = require('jsonwebtoken');
require('dotenv').config();
// const CustomApiError = require('../errors/custom-error');
const {BadRequestError} = require('../errors');
const login = async (req, res, next) => {
  const { username, password } = req.body;
  if (!username || !password) {
    throw new BadRequestError('plz provide email and pass');
  }
  const id = new Date().getDate();
  const token = jwt.sign({ id, username }, process.env.JWT, {
    expiresIn: '30d',
  });
  // console.log(username,password);
  res.status(200).json({ msg: 'user created', token });
};
const dashBoard = async (req, res, next) => {
  //   console.log('test');
  // console.log(req.headers);

  const luckyNumber = Math.floor(Math.random() * 100);
  res.status(200).json({
    msg: `hello ${req.user.username}`,
    secret: `ur lucky num is ${luckyNumber}`,
  });
};
module.exports = {
  login,
  dashBoard,
};
