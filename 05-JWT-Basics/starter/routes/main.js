const express = require('express');
const router = express.Router();
const authMiddleWare=require('../middleware/auth')
const { login, dashBoard } = require('../controllers/main');
router.route('/dashboard').get(authMiddleWare,dashBoard);
router.route('/login').post(login);
module.exports = router;
