const express = require('express');
const app = express();
const connectDB = require('./db/connect');
require('dotenv').config();

const tasksRoutes = require('./routes/tasks');
const notFound = require('./middleware/not-found');
const errorHandler = require('./middleware/error-handler');

//middlewares
app.use(express.static('./public'));
app.use(express.json()); //allow req.body

app.use('/api/v1/tasks', tasksRoutes);

app.use(notFound);
app.use(errorHandler);

const port = process.env.PORT;
const start = async () => {
  try {
    await connectDB(process.env.MONGO_URL);
    app.listen(port, console.log('server is listening on port ' + port));
  } catch (err) {
    console.log(err);
  }
};
start();
