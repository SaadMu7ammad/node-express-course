const asyncWrapper = require('../middleware/async');
const Task = require('../models/task');
const {createCustomError}=require('../errors/custom-error')
const getAllTasks = asyncWrapper(async (req, res) => {
  // try {

  const tasks = await Task.find({});
  // res.status(200).json({ tasks,amount:tasks.length });
  // res.status(200).json({ tasks,amount:tasks.length });
  res
    .status(200)
    .json({ status: 'success', data: { tasks, nbHits: tasks.length } });
  // } catch (err) {
  //   // console.log(err);
  //   res.status(500).json({ msg: err });
  // }
});
const createTask = asyncWrapper(async (req, res) => {
  // res.send('create task');
  // try {
  const task = await Task.create(req.body);
  res.status(201).json({ task });
  // } catch (err) {
  // console.log(err);
  // res.status(500).json({ msg: err });
  // }
});
const getTask = asyncWrapper(async (req, res, next) => {
  // try {
  const { id: taskId } = req.params;
  const task = await Task.findOne({ _id: taskId });
  if (task) {
    //   return res.status(200).json({ task });
    return res.status(200).json({ task: task, status: 'success' });
  } else {
    // const error = new Error('not found');
    // error.statusCode = 404;
    // throw error;
    // return next(error);//the same
    return next(createCustomError(`not found task with id ${taskId}`,404))
    // return res.status(404).json({ msg: `not found task with id ${taskId}` });
  }
  // } catch (err) {
  //   res.status(500).json({ msg: err });
  // }
});
const updateTask = asyncWrapper(async (req, res) => {
  // try {
  const { id: taskId } = req.params;
  const task = await Task.findOneAndUpdate({ _id: taskId }, req.body, {
    new: true,
    runValidators: true,
  });
  if (task) {
    return res.status(200).json({ task });
  } else {
    return next(createCustomError(`not found task with id ${taskId}`,404))

    // return res.status(404).json({ msg: `not found task with id ${taskId}` });
  }
  // } catch (err) {
  //   res.status(500).json({ msg: err });
  // }
});

const deleteTask = asyncWrapper(async (req, res) => {
  // try {
  const { id: taskId } = req.params;
  const task = await Task.findOneAndDelete({ _id: taskId });
  if (task) {
    res.status(200).json({ task });
  } else {
    return next(createCustomError(`not found task with id ${taskId}`,404))

    // return res.status(404).json({ msg: `not found task with id ${taskId}` });
  }
  // } catch (err) {
  //   res.status(500).json({ msg: err });
  // }
});
module.exports = {
  getAllTasks,
  createTask,
  getTask,
  updateTask,
  deleteTask,
};
